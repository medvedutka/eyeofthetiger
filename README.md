Cette application est une aide à la gestion de courses pédestre (marathon, endurance, demi-fond ...).

Elle vous permet:
 * De définir des participants (numéro, groupes, imports de fichier texte)
 * Imprimer les dossards
 * Créer des courses et enregistrer l'arrivé des participants à l'aide d'un lecteur de code barre (type "douchette" par exemple).

Les résultats sont sauvegardés sous un simple format CSV (ou exportés au format .xls) permettant le traitement ultérieur depuis un tableur.

*Nouveau! Version 20141116*: 
[NouvellesFonctionnalites Découvrez les nouvelle fonctionalité de cette versions].
 * Nouveau menu regroupant toutes les fonctionnalités des courses
 * Aide contextuelle pour les champs des options des dossards
 * Tris sur les Groupes ou sur les Renseignements
 * Amélioration diverses: sélecteurs de fichier, aide contextuelle, boites d'options.

Accédez à la zone de *[Telechargements téléchargement]*

http://wiki.eyeofthetiger.googlecode.com/git/images/screenshot.png